package practica5;


import java.nio.charset.Charset;
import javax.swing.JOptionPane;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.NoSuchMethodException;
import javax.swing.ButtonModel;

//sirven para conectarse con la base de datos
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;

public class P5MiniEncuesta extends javax.swing.JFrame {

    //private Conexion conexion = new Conexion();
    private Connection con = null;

    public P5MiniEncuesta() {
        initComponents();
        setLocationRelativeTo(null);
        setVisible(true);

        bGSO.add(rbLinux);
        bGSO.add(rbMac);
        bGSO.add(rbWindows);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bGSO = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        rbWindows = new javax.swing.JRadioButton();
        rbLinux = new javax.swing.JRadioButton();
        rbMac = new javax.swing.JRadioButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        cbProgra = new javax.swing.JCheckBox();
        cbDG = new javax.swing.JCheckBox();
        cbAdmin = new javax.swing.JCheckBox();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jSlider1 = new javax.swing.JSlider();
        bGenerar = new javax.swing.JButton();
        labelSlider = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Elige un sistema operativo");

        rbWindows.setText("Windows");
        rbWindows.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbWindowsActionPerformed(evt);
            }
        });

        rbLinux.setText("Linux");

        rbMac.setText("Mac");

        jLabel2.setText("Elige tu especialidad");

        cbProgra.setText("Programación");
        cbProgra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbPrograActionPerformed(evt);
            }
        });

        cbDG.setText("Diseño gráfico");

        cbAdmin.setText("Administración");

        jLabel3.setText("Horas que dedicas en el ordenador");

        jSlider1.setMaximum(24);
        jSlider1.setValue(12);
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });

        bGenerar.setText("Generar");
        bGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bGenerarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addComponent(jSeparator2)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(8, 8, 8)
                                        .addComponent(jLabel2))
                                    .addComponent(cbDG)
                                    .addComponent(cbProgra)
                                    .addComponent(cbAdmin)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(jLabel3))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(labelSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 33, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbMac)
                            .addComponent(rbLinux)
                            .addComponent(rbWindows)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addComponent(bGenerar)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(rbWindows)
                .addGap(18, 18, 18)
                .addComponent(rbLinux)
                .addGap(18, 18, 18)
                .addComponent(rbMac)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(cbProgra)
                .addGap(18, 18, 18)
                .addComponent(cbDG)
                .addGap(18, 18, 18)
                .addComponent(cbAdmin)
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(bGenerar)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbWindowsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbWindowsActionPerformed

    }//GEN-LAST:event_rbWindowsActionPerformed

    private void cbPrograActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbPrograActionPerformed


    }//GEN-LAST:event_cbPrograActionPerformed

    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged

        labelSlider.setText(Integer.toString(jSlider1.getValue()));

    }//GEN-LAST:event_jSlider1StateChanged

    private void bGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bGenerarActionPerformed

        String sOperativo = "";
        String progra = "N";
        String dGrafico = "N";
        String admin = "N";
        String hrsOrdenador = "";

        if (cbProgra.isSelected()) {
            progra = "S";
        }
        if (cbDG.isSelected()) {
            dGrafico = "S";
        }
        if (cbAdmin.isSelected()) {
            admin = "S";
        }
        if (rbMac.isSelected()) {
            sOperativo = "Mac";
        }
        if (rbWindows.isSelected()) {
            sOperativo = "Windows";
        }
        if (rbLinux.isSelected()) {
            sOperativo = "Linux";
        }

        hrsOrdenador = labelSlider.getText();

//        String e1 = cbProgra.isSelected() ? "S":"N";
//        String e2 = cbDG.isSelected() ? "S":"N";    
//        String e3 = cbAdmin.isSelected() ? "S":"N";
        String resultado = "";
//        dos formas de concatenar respuestas a traves de cadenas
        resultado = sOperativo + "," + progra + "," + admin + "," + dGrafico + "," + hrsOrdenador;

        resultado = String.format("%s,%s,%s,%s,%s", sOperativo, progra, admin, dGrafico, hrsOrdenador);

//        pasamos la variable al metodo guardar resultado del metodo guadarResultadoBD
        guardarResultado(resultado);

        guardarResultadoDB(sOperativo, progra, dGrafico, admin, Integer.parseInt(hrsOrdenador));
        //this.guardarResultadoDB(sOperativo, progra, admin, dGrafico, hrsOrdenador);

        JOptionPane.showMessageDialog(this, resultado, "Guardado exitosamente", JOptionPane.INFORMATION_MESSAGE);

    }//GEN-LAST:event_bGenerarActionPerformed

    //GUARDAR RESULTADOS EN UN DOCUMENTO DE EXCEL 
    private void guardarResultado(String sResult) {
        String sArchivo = "encuesta.csv";
        FileWriter fw;

        try {
            fw = new FileWriter(sArchivo, Charset.forName("UTF-8"), true);
            fw.write(sResult + "\n");
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(P5MiniEncuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void guardarResultadoDB(String sSisOper, String sProgra, String sDiseno, String sAdmon, int horas) {
        Statement stmt;
        String sInsertStmt;

        sInsertStmt = String.format("INSERT INTO respuestas (sisoper,prog,diseno,admon,horas) VALUES ('%s','%s','%s','%s',%d)", sSisOper, sProgra, sDiseno, sAdmon, horas);

        System.out.println(sInsertStmt);

        try {
            if (con == null) {
                //direccion ip mas nombre del port
                //se creo un usuario con y se reemplazo % por la ip
                con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/encuesta?"
                        + "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&user=encuesta&password=encuesta");

                /*
                ﻿192.168.64.2 vm
            Connection con = DriverManager.getConnection( "jdbc:mysql://localhost:3306/DBname", "root", "root");
 
            Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/DBname", "root", "root");    
                 */
//                con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/encuesta", "encuesta", "encuesta");
            }
            stmt = con.createStatement();
            stmt.execute(sInsertStmt);

        } catch (SQLException ex) {
            Logger.getLogger(P5MiniEncuesta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(P5MiniEncuesta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(P5MiniEncuesta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(P5MiniEncuesta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(P5MiniEncuesta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //el driver no se carga por si solo, hay que poner el siguiente codigo
        //cargar la clase del driver de la libreria del driver MySQL
        try {
            Class.forName("com.mysql.jdbc.Driver").getDeclaredConstructor().newInstance();
        } catch (Exception ex) {
            // handle the error
            System.out.println(ex.getMessage());
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new P5MiniEncuesta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bGSO;
    private javax.swing.JButton bGenerar;
    private javax.swing.JCheckBox cbAdmin;
    private javax.swing.JCheckBox cbDG;
    private javax.swing.JCheckBox cbProgra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JLabel labelSlider;
    private javax.swing.JRadioButton rbLinux;
    private javax.swing.JRadioButton rbMac;
    private javax.swing.JRadioButton rbWindows;
    // End of variables declaration//GEN-END:variables
}
