package practica6;


import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

//CUANDO HABLAMOS DE MODELO, NOS REFERIMOS A LA FUENTE DE LOS DATOS
//Donde el componente view va a sacar los datos para hacer desplegados en la interfaz grafica
public class P6PeliculasModelo extends javax.swing.JFrame {

    DefaultComboBoxModel cbm;

    public P6PeliculasModelo() {
        initComponents();
        this.cbm = (DefaultComboBoxModel) this.jComboBox1.getModel();

        //getModel() devuelve un comoBoxModel(), tenemos que declarar un objeto de esta clase
        //comboBoxModel() es una clase que no se puede instanciar porque es una interfaz
        //se utiliza defaultComboBoxmodel()
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        campoPelicula = new javax.swing.JTextField();
        bAñadir = new javax.swing.JButton();
        bSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Escribe el título de la película");

        jLabel2.setText("Película");

        campoPelicula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoPeliculaActionPerformed(evt);
            }
        });

        bAñadir.setText("Añadir");
        bAñadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAñadirActionPerformed(evt);
            }
        });

        bSalir.setText("Salir");
        bSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(84, 84, 84)
                .addComponent(bAñadir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bSalir)
                .addGap(93, 93, 93))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(40, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(campoPelicula, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(43, 43, 43))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(campoPelicula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bAñadir)
                    .addComponent(bSalir))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void campoPeliculaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoPeliculaActionPerformed

    }//GEN-LAST:event_campoPeliculaActionPerformed

    private void bSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSalirActionPerformed

        System.exit(0);
    }//GEN-LAST:event_bSalirActionPerformed

    public boolean validarRepetidas(String nombrePeli) {
//        int contadorItems = this.jComboBox1.getItemCount();
        int contadorItems = this.cbm.getSize();
        for (int i = 0; i < jComboBox1.getItemCount(); i++) {
//            if (nombrePeli == jComboBox1.getItemAt(i)) {
            if (this.cbm.getElementAt(i).toString().equals(nombrePeli)) {
// con el model tenemos mas metodos disponibles e inserta objetos. Podemos agregar cualquier cosa, enteros, doubles, imagenes 
// el componente solo acepta agregar strings
                return true;
            }
            break;
        }
        return false;
    }
    private void bAñadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAñadirActionPerformed

        String nombrePelicula = this.campoPelicula.getText();
        if (nombrePelicula.length() == 0) {
            JOptionPane.showMessageDialog(null, "Ingrese la informacion correspondiente en el campo.");
        } else if (validarRepetidas(nombrePelicula) == true) {
            JOptionPane.showInternalMessageDialog(null, "La pelicula " + nombrePelicula + " ya existe. Intente nuevamente.");
        } else {
            jComboBox1.addItem(nombrePelicula);
        }
    }//GEN-LAST:event_bAñadirActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new P6PeliculasModelo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAñadir;
    private javax.swing.JButton bSalir;
    private javax.swing.JTextField campoPelicula;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
